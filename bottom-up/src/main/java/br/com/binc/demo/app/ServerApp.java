package br.com.binc.demo.app;

import br.com.binc.demo.service.impl.HelloWorldServiceImpl;

import javax.xml.ws.Endpoint;

public class ServerApp {

  public static void main(String[] args) {
    Endpoint.publish("http://localhost:9980/helloworld", new HelloWorldServiceImpl());
    System.out.println("Hello World Services Started!");
  }

}
