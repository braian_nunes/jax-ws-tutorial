package br.com.binc.demo.service.impl;

import br.com.binc.demo.model.TopNews;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class HelloWorldServiceImpl {

  @WebMethod
  public String hello(@WebParam(name = "name") String name) {
    return "Hello " + name + "!";
  }

  @WebMethod
  public TopNews getTopNews() {
    TopNews tn = new TopNews();
    tn.setHighlights("Braian Nunes published an example for Jax-ws tutorial.");
    tn.setTitle("Jax-WS Tutorial is Available");
    return tn;
  }

}
